package cat.itb.grid_listview.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cat.itb.grid_listview.R;
import cat.itb.grid_listview.models.UserProfile;

public class SuperAdapter extends BaseAdapter
{
    private Context context;
    private int layout;
    private ArrayList<UserProfile> names;

    public SuperAdapter(Context context, int layout, ArrayList<UserProfile> names)
    {
        this.context = context;
        this.layout = layout;
        this.names = names;
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public Object getItem(int position) {
        return names.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;

        if(convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(this.layout, null);

            holder = new ViewHolder();

            holder.usernameTextView = convertView.findViewById(R.id.userNameView);
            holder.userLvlTextView = convertView.findViewById(R.id.userLvlView);
            holder.profilePictureView = convertView.findViewById(R.id.profileImgView);

            convertView.setTag(holder);
        }
        else  holder = (ViewHolder) convertView.getTag();

        holder.usernameTextView.setText(names.get(position).getUserName());
        holder.userLvlTextView.setText(String.valueOf(names.get(position).getLvl()));
        holder.profilePictureView.setImageResource(names.get(position).getPicture());

        return convertView;
    }

    static class ViewHolder
    {
        private TextView usernameTextView;
        private TextView userLvlTextView;
        private ImageView profilePictureView;
    }
}
