package cat.itb.grid_listview.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import cat.itb.grid_listview.R;
import cat.itb.grid_listview.models.UserProfile;

import static cat.itb.grid_listview.activities.MainActivity.profiles;

public class AddProfileActivity extends AppCompatActivity
{
    private int positionEdit;
    private boolean isEditing = false;
    private final int[] images = {R.drawable.arrow, R.drawable.clown, R.drawable.devil, R.drawable.ghost, R.drawable.mummy, R.drawable.vampire, R.drawable.werewolf, R.drawable.zombie};

    private EditText userNameEdit, lvlEdit;
    private Spinner spinImages;
    private ImageView currentPicture;

    private void addNewUserProfile()
    {
        String userName = userNameEdit.getText().toString();
        String lvl = lvlEdit.getText().toString();
        UserProfile newUser = new UserProfile(
                userName.isEmpty()? "Anonymous" : userName,
                lvl.isEmpty()? 1 : Integer.parseInt(lvl),
                images[spinImages.getSelectedItemPosition()]);

        if (!isEditing) profiles.add(newUser);
        else profiles.set(positionEdit, newUser);

        finish();
    }

    private int findImageFromRef(int imageRef)
    {
        int position = 0;
        for (int image : images)
        {
            if(image == imageRef) return position;
            position++;
        }
        return position;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_profile);

        lvlEdit = findViewById(R.id.userLvlEdit);
        userNameEdit = findViewById(R.id.userNameEdit);
        currentPicture = findViewById(R.id.profileNewImg);

        spinImages = findViewById(R.id.imagesToSelectView);
        findViewById(R.id.addUserButton).setOnClickListener(new View.OnClickListener() { @Override public void onClick(View v) { addNewUserProfile(); }});

        spinImages.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) { currentPicture.setImageResource(images[position]); }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { currentPicture.setImageResource(images[R.drawable.ic_new_user]); }
        });

        Bundle values = getIntent().getExtras();

        if (values != null)
        {
            userNameEdit.setText(values.getString("name"));
            lvlEdit.setText(String.valueOf(values.getInt("lvl")));
            currentPicture.setImageResource(values.getInt("image"));
            spinImages.setSelection(findImageFromRef(values.getInt("image")));
            positionEdit = values.getInt("position");
            isEditing = true;
        }
    }
}