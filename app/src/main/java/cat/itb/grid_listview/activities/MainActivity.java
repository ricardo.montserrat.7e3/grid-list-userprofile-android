package cat.itb.grid_listview.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;

import cat.itb.grid_listview.R;
import cat.itb.grid_listview.adapter.SuperAdapter;
import cat.itb.grid_listview.models.UserProfile;

public class MainActivity extends AppCompatActivity
{
    private SuperAdapter adapter;

    private GridView gridView;
    private ListView listView;

    public static ArrayList<UserProfile> profiles;

    private void sendProfileData(int position)
    {
        Intent intent = new Intent(MainActivity.this, AddProfileActivity.class);
        UserProfile selected = profiles.get(position);
        intent.putExtra("name", selected.getUserName());
        intent.putExtra("lvl", selected.getLvl());
        intent.putExtra("image", selected.getPicture());
        intent.putExtra("position", position);
        startActivity(intent);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);
        gridView = findViewById(R.id.gridView);

        profiles = new ArrayList<UserProfile>() {{ add(new UserProfile("Username", 10, R.drawable.arrow)); }};

        adapter = new SuperAdapter(this, R.layout.user_profile_layout_list, profiles);

        listView.setAdapter(adapter);
        gridView.setAdapter(adapter);

        gridView.setVisibility(View.GONE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) { sendProfileData(position); }});

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) { sendProfileData(position); }});

        registerForContextMenu(listView);
        registerForContextMenu(gridView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(profiles.get(info.position).getUserName());

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId())
        {
            case R.id.delete_item:
                profiles.remove(info.position);
                adapter.notifyDataSetChanged();
                return true;
            case R.id.edit_item:
                sendProfileData(info.position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.optionMenuAddProfiles:
                startActivity(new Intent(MainActivity.this, AddProfileActivity.class));
                return true;
            case R.id.optionMenuChangeViewType:
                boolean isListVisible = listView.getVisibility() == View.VISIBLE;

                adapter = new SuperAdapter(MainActivity.this, isListVisible? R.layout.user_profile_layout_grid : R.layout.user_profile_layout_list, profiles);

                listView.setAdapter(adapter);
                gridView.setAdapter(adapter);

                listView.setVisibility(isListVisible?View.GONE:View.VISIBLE);
                gridView.setVisibility(isListVisible?View.VISIBLE:View.GONE);

                item.setIcon(isListVisible? R.drawable.ic_grid : R.drawable.ic_list);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}