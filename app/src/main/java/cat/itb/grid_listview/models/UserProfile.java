package cat.itb.grid_listview.models;

public class UserProfile
{
    private String userName;
    private int lvl, pictureRef;

    public UserProfile(String userName, int lvl, int pictureRef)
    {
        this.userName = userName;
        this.lvl = lvl;
        this.pictureRef = pictureRef;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getPicture() {
        return pictureRef;
    }

    public void setPicture(int picture) {
        this.pictureRef = picture;
    }
}
